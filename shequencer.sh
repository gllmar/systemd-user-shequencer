#!/bin/bash
SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
echo "$1"

for f in "$1"/*.sh; do
  echo "exec $f"
  bash "$f" -H || break  
done
