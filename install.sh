SERVICE_NAME=shequencer
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
echo "====> executable service sh"
chmod +x "$DIR"/$SERVICE_NAME.sh

echo "====> copying $SERVICE_NAME to /usr/local/bin " 
sudo cp  "$DIR"/$SERVICE_NAME.sh /usr/local/bin/$SERVICE_NAME

echo "====> copying service to user service folder"

mkdir -p ~/.config/systemd/user/
cp "$DIR"/$SERVICE_NAME.service ~/.config/systemd/user/$SERVICE_NAME.service
echo "====> reloading systemd daemon"
systemctl --user daemon-reload
